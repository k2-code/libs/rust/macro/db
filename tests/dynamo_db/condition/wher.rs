use db_macro::where_cond;
use traits::db::dynamo_db::IntoScanExpr;

#[where_cond]
#[derive(Debug)]
enum Where {
    TestField(String),
}

#[test]
fn into_scan_expr_implemented() {
    trait_checker(Where::TestField("test_field".to_string()));
}

fn trait_checker<T>(_cond: T)
where
    T: IntoScanExpr,
{
}
