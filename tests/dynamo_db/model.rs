use crate::{
    BoolModel, JsonModel, StringModel, U16Model, U32Model, U8Model, USizeModel, VecStringModel,
};
use aws_sdk_dynamodb::model::AttributeValue;
use std::collections::HashMap;
use traits::db::dynamo_db::IntoPutItem;

#[test]
fn from_u8_db_item_works() {
    let db_item: HashMap<String, AttributeValue> =
        HashMap::from([("field".to_string(), AttributeValue::N("8".to_string()))]);

    assert!(U8Model::from(db_item).field.is_some());
}

#[test]
fn from_u16_db_item_works() {
    let db_item: HashMap<String, AttributeValue> =
        HashMap::from([("field".to_string(), AttributeValue::N("16".to_string()))]);

    assert!(U16Model::from(db_item).field.is_some());
}

#[test]
fn from_u32_db_item_works() {
    let db_item: HashMap<String, AttributeValue> =
        HashMap::from([("field".to_string(), AttributeValue::N("32".to_string()))]);

    assert!(U32Model::from(db_item).field.is_some());
}

#[test]
fn from_usize_db_item_works() {
    let db_item: HashMap<String, AttributeValue> =
        HashMap::from([("field".to_string(), AttributeValue::N("123".to_string()))]);

    assert!(USizeModel::from(db_item).field.is_some());
}

#[test]
fn from_string_db_item_works() {
    let db_item: HashMap<String, AttributeValue> =
        HashMap::from([("field".to_string(), AttributeValue::S("String".to_string()))]);

    assert!(StringModel::from(db_item).field.is_some());
}

#[test]
fn from_bool_db_item_works() {
    let db_item: HashMap<String, AttributeValue> =
        HashMap::from([("field".to_string(), AttributeValue::Bool(true))]);

    assert!(BoolModel::from(db_item).field.is_some());
}

#[test]
fn from_vector_db_item_works() {
    let db_item: HashMap<String, AttributeValue> = HashMap::from([(
        "field".to_string(),
        AttributeValue::Ss(vec![
            "this".to_string(),
            "is".to_string(),
            "test".to_string(),
        ]),
    )]);

    let model: VecStringModel = VecStringModel::from(db_item);

    assert!(model.field.is_some());
    assert_eq!(3, model.field.unwrap().len());
}

#[test]
fn from_json_db_item_works() {
    let db_item: HashMap<String, AttributeValue> = HashMap::from([(
        "field".to_string(),
        AttributeValue::S("{\"from\":2,\"to\":4}".to_string()),
    )]);

    assert!(JsonModel::from(db_item).field.is_some());
}

#[test]
fn from_string_created_at_is_some() {
    let db_item: HashMap<String, AttributeValue> = HashMap::from([(
        "created_at".to_string(),
        AttributeValue::S("2022-11-18T12:00:09Z".to_string()),
    )]);

    assert!(StringModel::from(db_item).created_at().is_some());
}

#[test]
fn from_string_created_at_is_none() {
    let db_item: HashMap<String, AttributeValue> = HashMap::from([(
        "created_at".to_string(),
        AttributeValue::S("wrong date".to_string()),
    )]);

    assert!(StringModel::from(db_item).created_at().is_none());
}

#[test]
fn from_string_updated_at_is_some() {
    let db_item: HashMap<String, AttributeValue> = HashMap::from([(
        "updated_at".to_string(),
        AttributeValue::S("2022-11-18T12:00:09Z".to_string()),
    )]);

    assert!(StringModel::from(db_item).updated_at().is_some());
}

#[test]
fn from_string_updated_at_is_none() {
    let db_item: HashMap<String, AttributeValue> = HashMap::from([(
        "updated_at".to_string(),
        AttributeValue::S("wrong date".to_string()),
    )]);

    assert!(StringModel::from(db_item).updated_at().is_none());
}

#[test]
fn into_put_item_implemented() {
    trait_checker(U8Model::new(Some(8)));
}

fn trait_checker<T>(_model: T)
where
    T: IntoPutItem,
{
}
