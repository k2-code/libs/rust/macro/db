use crate::NoUUIDModel;

#[test]
fn no_uuid_works() {
    assert!(NoUUIDModel::default().id().is_none())
}
