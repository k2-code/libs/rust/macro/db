use crate::{SlugManualModel, SlugModel};

const SLUG: &str = "hello-world";

#[test]
fn generate_slug_works() {
    let mut model: SlugModel = SlugModel::default();

    model.set_title("Hello World!");

    assert!(model.slug().as_ref().unwrap().contains(SLUG));
}

#[test]
fn slug_manual_works() {
    let mut model: SlugManualModel = SlugManualModel::default();

    model.set_slug(SLUG);

    assert_eq!(SLUG, model.slug().as_ref().unwrap());
}
