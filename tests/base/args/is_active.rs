use crate::ActiveModel;

#[test]
fn is_activated_works() {
    assert_eq!(false, ActiveModel::default().is_activated());
}

#[test]
fn is_exists_works() {
    assert_eq!(false, ActiveModel::default().is_exists());
}

#[test]
fn activate_works() {
    assert!(ActiveModel::default().activate().is_activated());
}

#[test]
fn deactivate_works() {
    let mut model = ActiveModel::default();

    model.activate();

    assert!(model.is_activated());

    model.deactivate(types::db::DeactivateReason::ByUser);

    assert_eq!(false, model.is_activated());
    assert_eq!(
        &Some(types::db::DeactivateReason::ByUser.to_string()),
        model.deactivate_reason()
    );
}

#[test]
fn new_active_works() {
    assert!(ActiveModel::new_active().is_activated());
}
