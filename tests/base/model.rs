use crate::U8Model;
use chrono::{DateTime, Utc};
use traits::db::Table;

#[test]
fn constructor_works() {
    assert_eq!(U8Model::new(Some(8)).field, Some(8));
}

#[test]
fn default_works() {
    assert!(U8Model::default().id().is_some());
}

#[test]
fn getter_works() {
    assert_eq!(U8Model::new(Some(8)).field(), &Some(8));
}

#[test]
fn setter_works() {
    let mut model = U8Model::new(Some(8));

    model.set_field(5);

    assert_eq!(model.field(), &Some(5));
}

#[test]
fn additional_fields_getters_works() {
    let model = U8Model::new(Some(8));

    assert!(model.id().is_some());
    assert!(model.created_at().is_some());
    assert!(model.updated_at().is_some());
}

#[test]
fn additional_fields_setters_works() {
    let mut model = U8Model::new(Some(8));

    let id: Option<String> = model.id().to_owned();
    let created_at: Option<DateTime<Utc>> = model.created_at().to_owned();
    let updated_at: Option<DateTime<Utc>> = model.updated_at().to_owned();

    model.set_id("test_id");
    model.set_created_at(Utc::now());

    assert_ne!(&id, model.id());
    assert_ne!(&created_at, model.created_at());

    assert_ne!(&updated_at, model.updated_at());
}

#[test]
fn chained_calls_works() {
    let mut model = U8Model::new(Some(8));

    let test_id: &str = "test_id";

    model.set_id(test_id).set_created_at(Utc::now());

    assert_eq!(&Some(test_id.to_string()), model.id());
}

#[test]
fn table_trait_implemented() {
    U8Model::table();
}

#[test]
fn model_table_works() {
    assert_eq!(U8Model::table(), "u_8_models");
}
