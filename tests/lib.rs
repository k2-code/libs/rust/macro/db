mod base;
#[cfg(feature = "dynamo_db")]
mod dynamo_db;

use db_macro::model;
use serde::{Deserialize, Serialize};
use types::db::JsonField;

#[model(no_uuid)]
#[derive(Debug)]
struct NoUUIDModel {
    field: Option<String>,
}

#[model(is_active)]
#[derive(Debug)]
struct ActiveModel {
    field: Option<u8>,
}

#[model(slug)]
#[derive(Debug)]
struct SlugModel {
    title: Option<String>,
}

#[model(slug(unique))]
#[derive(Debug)]
struct SlugUniqueModel {
    title: Option<String>,
}

#[model]
#[derive(Debug)]
struct SlugManualModel {
    slug: Option<String>,
}

#[model]
#[derive(Debug)]
pub struct U8Model {
    field: Option<u8>,
}

#[model]
#[derive(Debug)]
struct U16Model {
    field: Option<u16>,
}

#[model]
#[derive(Debug)]
struct U32Model {
    field: Option<u32>,
}

#[model]
#[derive(Debug)]
struct USizeModel {
    field: Option<usize>,
}

#[model]
#[derive(Debug)]
struct StringModel {
    field: Option<String>,
}

#[model]
#[derive(Debug)]
struct BoolModel {
    field: Option<bool>,
}

#[model]
#[derive(Debug)]
pub struct I8Model {
    field: Option<i8>,
}

#[model]
#[derive(Debug)]
struct I16Model {
    field: Option<i16>,
}

#[model]
#[derive(Debug)]
struct I32Model {
    field: Option<i32>,
}

#[model]
#[derive(Debug)]
struct F32Model {
    field: Option<f32>,
}

#[model]
#[derive(Debug)]
struct F64Model {
    field: Option<f64>,
}

#[model]
#[derive(Debug)]
struct VecStringModel {
    field: Option<Vec<String>>,
}

#[model]
#[derive(Debug)]
struct JsonModel {
    field: Option<JsonField<TestJsonField>>,
}

#[derive(Debug, Serialize, Deserialize)]
struct TestJsonField {
    from: u8,
    to: u8,
}
