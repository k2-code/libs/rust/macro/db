mod is_active;
mod slug;

use proc_macro2::TokenStream;
use quote::quote;

pub const ARG_IS_ACTIVE: &str = "is_active";
pub const ARG_NO_UUID: &str = "no_uuid";

pub const ARG_SLUG: &str = "slug";
pub const ARG_SLUG_UNIQUE: &str = "slug(unique)";

pub fn expand(args: &[String]) -> TokenStream {
    let is_active_impls: TokenStream = is_active::expand(args);
    let slug_impls: TokenStream = slug::expand(args);

    quote! {
        #is_active_impls
        #slug_impls
    }
}
