use crate::is_arg_set;
use crate::model::args::{ARG_SLUG, ARG_SLUG_UNIQUE};
use proc_macro2::TokenStream;
use quote::quote;

pub fn expand(args: &[String]) -> TokenStream {
    if is_arg_set(args, ARG_SLUG) {
        return quote! {
            fn generate_slug(&mut self) -> &mut Self {
                self.slug = Some(
                    slug::slugify(self.title().as_ref().unwrap())
                );

                self
            }
        };
    }

    if is_arg_set(args, ARG_SLUG_UNIQUE) {
        return quote! {
            fn generate_slug(&mut self) -> &mut Self {
                let mut slug = slug::slugify(self.title().as_ref().unwrap());

                slug.push_str("-");
                slug.push_str(
                    &self
                        .id()
                        .as_ref()
                        .unwrap()
                        .split("-")
                        .collect::<Vec<&str>>()[0][0..=4],
                );

                self.slug = Some(slug);
                self
            }
        };
    }

    quote!()
}
