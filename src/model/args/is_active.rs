use crate::is_arg_set;
use crate::model::args::ARG_IS_ACTIVE;
use proc_macro2::TokenStream;
use quote::quote;

pub fn expand(args: &[String]) -> TokenStream {
    if is_arg_set(args, ARG_IS_ACTIVE) {
        return quote! {
            pub fn is_exists(&self) -> bool {
                self.is_active.is_some()
            }

            pub fn is_activated(&self) -> bool {
                self.is_active == Some(1)
            }

            pub fn activate(&mut self) -> &mut Self {
                self.is_active = Some(1);
                self
            }

            pub fn deactivate(&mut self, reason: types::db::DeactivateReason) -> &mut Self {
                self.is_active = Some(0);
                self.deactivate_reason = Some(reason.to_string());

                self
            }

            pub fn new_active() -> Self {
                let mut item = Self::default();

                item.is_active = Some(1);
                item
            }
        };
    }

    quote!()
}
