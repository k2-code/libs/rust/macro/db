use crate::is_arg_set;
use crate::model::args::{ARG_IS_ACTIVE, ARG_SLUG, ARG_SLUG_UNIQUE};
use crate::model::NamedFields;
use proc_macro2::{Ident, TokenStream};
use quote::{quote, ToTokens};
use syn::parse::Parser;
use syn::{
    AngleBracketedGenericArguments as ABGA, Fields, FieldsNamed, ItemStruct, PathArguments, Type,
    TypePath,
};

pub fn expand(item_struct: &mut ItemStruct, args: &[String]) -> TokenStream {
    if let Fields::Named(ref mut fields) = item_struct.fields {
        fields.named.push(
            syn::Field::parse_named
                .parse2(quote! {
                    id: Option<String>
                })
                .unwrap(),
        );

        fields.named.push(
            syn::Field::parse_named
                .parse2(quote! {
                    created_at: Option<chrono::DateTime<chrono::Utc>>
                })
                .unwrap(),
        );

        fields.named.push(
            syn::Field::parse_named
                .parse2(quote! {
                    updated_at: Option<chrono::DateTime<chrono::Utc>>
                })
                .unwrap(),
        );

        process_args(args, fields);
    }

    quote! {
        #item_struct
    }
}

pub fn get_generic_token(field_name: &str, field_type: &Type) -> Option<TokenStream> {
    let Type::Path(TypePath { path, .. }) = field_type else {
        return None;
    };

    assert_eq!(
        path.segments.first()?.ident.to_string(),
        "Option",
        "Try to wrap the field {} with an Option!",
        field_name
    );

    let PathArguments::AngleBracketed(ABGA { args, .. }) = &path.segments.first()?.arguments else {
        return None;
    };

    Some(args.first()?.to_token_stream())
}

pub fn get_user_fields_and_types<'a>(
    args: &'a [String],
    item_fields: &'a NamedFields,
) -> (Vec<&'a Ident>, Vec<&'a Type>) {
    let mut fields: Vec<&Ident> = vec![];
    let mut types: Vec<&Type> = vec![];

    let mut auto_fields: Vec<&str> = vec![
        "id",
        "created_at",
        "updated_at",
        "is_active",
        "deactivate_reason",
    ];

    process_user_fields_args(args, &mut auto_fields);

    for field in item_fields {
        let field_name: &str = &field.ident.as_ref().unwrap().to_string();

        if auto_fields.contains(&field_name) {
            continue;
        }

        fields.push(field.ident.as_ref().unwrap());
        types.push(&field.ty);
    }

    (fields, types)
}

fn process_user_fields_args(args: &[String], fields: &mut Vec<&str>) {
    if is_arg_set(args, ARG_SLUG) || is_arg_set(args, ARG_SLUG_UNIQUE) {
        fields.push("slug");
    }
}

fn process_args(args: &[String], fields: &mut FieldsNamed) {
    if is_arg_set(args, ARG_IS_ACTIVE) {
        fields.named.push(
            syn::Field::parse_named
                .parse2(quote! {
                    is_active: Option<u8>
                })
                .unwrap(),
        );

        fields.named.push(
            syn::Field::parse_named
                .parse2(quote! {
                    deactivate_reason: Option<String>
                })
                .unwrap(),
        );
    }

    if is_arg_set(args, ARG_SLUG) || is_arg_set(args, ARG_SLUG_UNIQUE) {
        fields.named.push(
            syn::Field::parse_named
                .parse2(quote! {
                    slug: Option<String>
                })
                .unwrap(),
        );
    }
}
