use convert_case::{Case, Casing};
use pluralize_rs::to_plural;
use proc_macro2::{Ident, TokenStream};
use quote::quote;

pub fn expand(name: &Ident) -> TokenStream {
    let title: String = to_plural(
        &name
            .to_string()
            .from_case(Case::Pascal)
            .to_case(Case::Snake),
    );

    quote! {
        impl traits::db::Table for #name {
            fn table() -> String {
                #title.to_string()
            }
        }
    }
}
