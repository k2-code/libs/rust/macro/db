use crate::is_arg_set;
use crate::model::args::{ARG_SLUG, ARG_SLUG_UNIQUE};
use crate::model::fields::get_generic_token;
use crate::model::NamedFields;
use proc_macro2::{Ident, Span, TokenStream};
use quote::quote;

pub fn expand(args: &[String], fields: &NamedFields) -> Vec<TokenStream> {
    let mut tokens: Vec<TokenStream> = vec![];

    for field in fields {
        let field_ident: &Ident = field.ident.as_ref().unwrap();
        let field_name: &str = &field_ident.to_string();
        let additional_calls_token: TokenStream = get_additional_calls(args, field_name);

        let mut auto_fields: Vec<&str> = vec!["is_active", "deactivate_reason", "updated_at"];

        process_user_fields_args(args, &mut auto_fields);

        if auto_fields.contains(&field_name) {
            continue;
        }

        let mut field_type_generic_token: TokenStream = get_generic_token(field_name, &field.ty)
            .unwrap_or_else(|| panic!("Cannot get generic type from the field {}", field_name));

        let mut field_setter_token: TokenStream = quote!(self.#field_ident = Some(value););

        if field_type_generic_token.to_string().eq("String") {
            field_type_generic_token = quote!(impl Into<String>);
            field_setter_token = quote!(self.#field_ident = Some(value.into()););
        }

        let setter_name: Ident = Ident::new(&format!("set_{}", field_name), Span::call_site());

        tokens.push(quote! {
            pub fn #setter_name(&mut self, value: #field_type_generic_token)  -> &mut Self {
                #field_setter_token
                #additional_calls_token

                self.update()
            }
        })
    }

    tokens
}

fn get_additional_calls(args: &[String], field_name: &str) -> TokenStream {
    if (is_arg_set(args, ARG_SLUG) || is_arg_set(args, ARG_SLUG_UNIQUE)) && field_name == "title" {
        return quote!(self.generate_slug(););
    }

    quote!()
}

fn process_user_fields_args(args: &[String], fields: &mut Vec<&str>) {
    if is_arg_set(args, ARG_SLUG) || is_arg_set(args, ARG_SLUG_UNIQUE) {
        fields.push("slug");
    }
}
