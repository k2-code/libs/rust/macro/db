mod table;

use proc_macro2::{Ident, TokenStream};
use quote::quote;
use syn::ItemStruct;

#[allow(unused_variables)]
pub fn expand(name: &Ident, item_struct: &ItemStruct) -> TokenStream {
    let table_trait: TokenStream = table::expand(name);

    quote! {
        #table_trait
    }
}
