use crate::is_arg_set;
use crate::model::args::{ARG_IS_ACTIVE, ARG_NO_UUID, ARG_SLUG, ARG_SLUG_UNIQUE};
use crate::model::fields::get_user_fields_and_types;
use crate::model::NamedFields;
use proc_macro2::{Ident, Span, TokenStream};
use quote::quote;
use syn::Type;

pub fn expand(args: &[String], item_fields: &NamedFields) -> TokenStream {
    let (fields, types): (Vec<&Ident>, Vec<&Type>) = get_user_fields_and_types(args, item_fields);
    let (id, is_active, slug): (TokenStream, TokenStream, TokenStream) = process_args(args);

    quote! {
        pub fn new(#(#fields: #types),*) -> Self {
            let current_utc = chrono::Utc::now();

            Self {
                #id,

                created_at: Some(current_utc),
                updated_at: Some(current_utc),

                #is_active
                #slug

                #(#fields),*
            }
        }
    }
}

pub fn get_fillers(args: &[String], item_fields: &NamedFields) -> Vec<Ident> {
    let (fields, _): (Vec<&Ident>, Vec<&Type>) = get_user_fields_and_types(args, item_fields);

    fields
        .iter()
        .map(|_| Ident::new("None", Span::call_site()))
        .collect()
}

fn process_args(args: &[String]) -> (TokenStream, TokenStream, TokenStream) {
    let mut id: TokenStream = quote!(id: None);
    let mut is_active: TokenStream = quote!();
    let mut slug: TokenStream = quote!();

    if !is_arg_set(args, ARG_NO_UUID) {
        id = quote!(id: Some(uuid::Uuid::new_v4().to_string()));
    }

    if is_arg_set(args, ARG_IS_ACTIVE) {
        is_active = quote! {
            is_active: None,
            deactivate_reason: None,
        };
    }

    if is_arg_set(args, ARG_SLUG) || is_arg_set(args, ARG_SLUG_UNIQUE) {
        slug = quote!(slug: None,);
    }

    (id, is_active, slug)
}
