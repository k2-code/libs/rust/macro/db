use crate::model::{FCSMap, NamedFields};
use proc_macro2::Ident;
use quote::quote;

pub fn expand(fields: &NamedFields) -> FCSMap {
    fields.iter().map(|f| {
        let field_name: &Option<Ident> = &f.ident;
        let field_ty: &syn::Type = &f.ty;

        quote! {
            pub fn #field_name(&self) -> &#field_ty {
                &self.#field_name
            }
        }
    })
}
