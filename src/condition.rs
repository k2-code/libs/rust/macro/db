pub mod wher;

use proc_macro2::TokenStream;
use std::iter::Map;
use syn::punctuated::{Iter, Punctuated};
use syn::token::Comma;
use syn::Variant;

#[allow(dead_code)]
pub type VCSMap<'a> = Map<Iter<'a, Variant>, fn(&Variant) -> TokenStream>;

#[allow(dead_code)]
pub type EnumVariants = Punctuated<Variant, Comma>;
