mod args;
mod constructor;
mod getters;
mod setters;
mod traits;

pub mod fields;

use crate::model::constructor::get_fillers;
use proc_macro2::{Ident, TokenStream};
use quote::quote;
use std::iter::Map;
use syn::punctuated::{Iter, Punctuated};
use syn::token::Comma;
use syn::{Field, ItemStruct};

pub type FCSMap<'a> = Map<Iter<'a, Field>, fn(&Field) -> TokenStream>;
pub type NamedFields = Punctuated<Field, Comma>;

#[allow(unused_variables)]
pub fn expand(item_struct: &ItemStruct, args: &[String]) -> TokenStream {
    let name: &Ident = &item_struct.ident;

    let item_struct_fields: &NamedFields = get_fields(item_struct);
    let constructor: TokenStream = constructor::expand(args, item_struct_fields);

    let getters: FCSMap = getters::expand(item_struct_fields);
    let setters: Vec<TokenStream> = setters::expand(args, item_struct_fields);

    let traits_impls: TokenStream = traits::expand(name, item_struct);
    let args_impls: TokenStream = args::expand(args);

    let dynamo_db_impls: TokenStream = quote!();
    #[cfg(feature = "dynamo_db")]
    let dynamo_db_impls: TokenStream = crate::dynamo_db::model::expand(name, item_struct_fields);

    let constructor_fillers: Vec<Ident> = get_fillers(args, item_struct_fields);

    quote! {
        #[automatically_derived]
        impl #name {
            #constructor

            pub fn default() -> Self {
                Self::new(#(#constructor_fillers),*)
            }

            #(#getters)*
            #(#setters)*

            #args_impls

            fn update(&mut self) -> &mut Self {
                self.updated_at = Some(chrono::Utc::now());
                self
            }
        }

        #traits_impls

        #dynamo_db_impls
    }
}

pub fn get_fields(item_struct: &ItemStruct) -> &NamedFields {
    let syn::Fields::Named(ref fields) = item_struct.fields else {
        panic!("this derive macro only works on structs with named fields")
    };

    &fields.named
}
