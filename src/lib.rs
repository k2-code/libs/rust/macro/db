mod condition;
mod model;

#[cfg(feature = "dynamo_db")]
mod dynamo_db;

use quote::quote;
use syn::{
    parse, parse_macro_input, AttributeArgs, ItemEnum, ItemStruct, Meta, MetaList, NestedMeta, Path,
};

#[proc_macro_attribute]
pub fn where_cond(
    args: proc_macro::TokenStream,
    input: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let item_enum: ItemEnum = parse_macro_input!(input as ItemEnum);
    let _ = parse_macro_input!(args as parse::Nothing);

    // TODO: uncomment when IDE can parse proc_macro additional fields and methods
    // https://github.com/intellij-rust/intellij-rust/issues/1786
    // https://intellij-rust.github.io/2022/12/05/changelog-184.html
    // condition::wher::fields::expand(&mut item_enum);

    let impls: proc_macro2::TokenStream = condition::wher::expand(&item_enum);

    (quote! {
        #item_enum
        #impls
    })
    .into()
}

#[proc_macro_attribute]
pub fn model(
    args: proc_macro::TokenStream,
    input: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
    let mut item_struct: ItemStruct = parse_macro_input!(input as ItemStruct);
    let args: Vec<String> = get_args(parse_macro_input!(args as AttributeArgs));

    model::fields::expand(&mut item_struct, &args);

    let impls: proc_macro2::TokenStream = model::expand(&item_struct, &args);

    (quote! {
        #item_struct
        #impls
    })
    .into()
}

fn is_arg_set(args: &[String], arg: &str) -> bool {
    args.iter().any(|a| a == arg)
}

fn get_args(args: AttributeArgs) -> Vec<String> {
    let mut arguments: Vec<String> = vec![];

    for arg in args {
        if let NestedMeta::Meta(Meta::Path(Path { segments, .. })) = arg {
            arguments.push(segments[0].ident.to_string());
            continue;
        }

        if let NestedMeta::Meta(Meta::List(MetaList { path, nested, .. })) = arg {
            let main_arg: String = path.segments[0].ident.to_string();
            let mut sub_args: Vec<String> = vec![];

            for nested_args in nested {
                if let NestedMeta::Meta(Meta::Path(Path { segments, .. })) = nested_args {
                    sub_args.push(segments[0].ident.to_string());
                    continue;
                }
            }

            arguments.push(format!("{}({})", main_arg, sub_args.join(", ")));
            continue;
        }
    }

    arguments
}
