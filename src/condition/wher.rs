pub mod fields;

use proc_macro2::{Ident, TokenStream};
use quote::quote;
use syn::ItemEnum;

#[allow(unused_variables)]
pub fn expand(item_enum: &ItemEnum) -> TokenStream {
    let name: &Ident = &item_enum.ident;

    let dynamo_db_impls: TokenStream = quote!();

    #[cfg(feature = "dynamo_db")]
    let dynamo_db_impls: TokenStream =
        crate::dynamo_db::condition::wher::expand(name, &item_enum.variants);

    quote! {
        #dynamo_db_impls
    }
}
