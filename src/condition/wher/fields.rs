use proc_macro2::TokenStream;
use quote::quote;
use syn::{parse_str, ItemEnum, Variant};

#[allow(dead_code)]
pub fn expand(item_enum: &mut ItemEnum) -> TokenStream {
    item_enum
        .variants
        .push(parse_str::<Variant>("Id(String)").unwrap());

    item_enum
        .variants
        .push(parse_str::<Variant>("CreatedAt(String)").unwrap());

    item_enum
        .variants
        .push(parse_str::<Variant>("UpdatedAt(String)").unwrap());

    quote! {
        #item_enum
    }
}
