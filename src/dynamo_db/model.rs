mod from_db_item;
mod into_put_item;

use crate::model::NamedFields;
use proc_macro2::{Ident, TokenStream};
use quote::quote;

pub fn expand(name: &Ident, item_fields: &NamedFields) -> TokenStream {
    let from_db_item: TokenStream = from_db_item::expand(name, item_fields);
    let into_put_item: TokenStream = into_put_item::expand(name, item_fields);

    quote! {
        #from_db_item
        #into_put_item
    }
}
