mod into_scan_expr;

use crate::condition::EnumVariants;
use proc_macro2::{Ident, TokenStream};
use quote::quote;

pub fn expand(name: &Ident, item_variants: &EnumVariants) -> TokenStream {
    let into_scan_expr: TokenStream = into_scan_expr::expand(name, item_variants);

    quote! {
        #into_scan_expr
    }
}
