use crate::condition::{EnumVariants, VCSMap};
use convert_case::{Case, Casing};
use proc_macro2::{Ident, TokenStream};
use quote::quote;

pub fn expand(name: &Ident, item_variants: &EnumVariants) -> TokenStream {
    let scan_value_setters: VCSMap = item_variants.iter().map(|v| {
        let variant_ident: &Ident = &v.ident;
        let variant_name: &str = &variant_ident
            .to_string()
            .from_case(Case::Pascal)
            .to_case(Case::Snake);

        quote! {
            Self::#variant_ident(value) => base_scan
                .filter_expression(
                    format!("{} {} :{}", #variant_name, operator, #variant_name)
                )
                .expression_attribute_values(
                    format!(":{}", #variant_name),
                    aws_sdk_dynamodb::model::AttributeValue::S(value.to_owned())
                ),
        }
    });

    quote! {
        impl traits::db::dynamo_db::IntoScanExpr for #name {
            fn into_scan_expr(self, base_scan: aws_sdk_dynamodb::client::fluent_builders::Scan, operator: impl std::fmt::Display) -> aws_sdk_dynamodb::client::fluent_builders::Scan {
                match self {
                    #(#scan_value_setters)*
                }
            }
        }
    }
}
