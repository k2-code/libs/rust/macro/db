use crate::model::fields::get_generic_token;
use crate::model::{FCSMap, NamedFields};
use proc_macro2::{Ident, Span, TokenStream};
use quote::quote;

pub fn expand(name: &Ident, item_fields: &NamedFields) -> TokenStream {
    let item_value_setters: FCSMap = item_fields.iter().map(|f| {
        let field_ident: &Ident = f.ident.as_ref().unwrap();
        let field_name: &str = &field_ident.to_string();

        let field_type_generic_token: TokenStream = get_generic_token(field_name, &f.ty)
            .unwrap_or_else(|| panic!("Cannot get generic type from the field {}", field_name));

        let field_type_generic_str: &str = &field_type_generic_token.to_string().replace(' ', "");

        let value_matcher: TokenStream = get_value_matcher(
            field_ident,
            field_type_generic_str,
            get_av_type_ident(
                field_type_generic_str
            )
        );

        quote! {
            let new_item: aws_sdk_dynamodb::client::fluent_builders::PutItem = new_item.item(#field_name, #value_matcher);
        }
    });

    quote! {
        impl traits::db::dynamo_db::IntoPutItem for #name {
             fn to_put_item(&self, new_item: aws_sdk_dynamodb::client::fluent_builders::PutItem) -> aws_sdk_dynamodb::client::fluent_builders::PutItem {
                #(#item_value_setters)*

                new_item
            }
        }
    }
}

fn get_value_matcher(
    field_ident: &Ident,
    field_type_generic_str: &str,
    av_type_ident: Ident,
) -> TokenStream {
    let value_getter: TokenStream = get_vm_value_getter(&av_type_ident, field_type_generic_str);

    quote! {
         match self.#field_ident() {
            None => aws_sdk_dynamodb::model::AttributeValue::Null(true),
            Some(value) => aws_sdk_dynamodb::model::AttributeValue::#av_type_ident(#value_getter),
        },
    }
}

fn get_vm_value_getter(av_type_ident: &Ident, field_type_generic_str: &str) -> TokenStream {
    if field_type_generic_str == "Vec<String>" {
        return quote!(value.to_vec());
    }

    if field_type_generic_str.contains("JsonField") {
        return quote!(serde_json::to_string(&value).unwrap());
    }

    match &*av_type_ident.to_string() {
        "Bool" => quote!(*value),
        _ => quote!(value.to_string()),
    }
}

fn get_av_type_ident(field_type_generic_str: &str) -> Ident {
    if field_type_generic_str.contains("JsonField") {
        return Ident::new("S", Span::call_site());
    }

    Ident::new(
        match field_type_generic_str {
            "String" | "chrono::DateTime<chrono::Utc>" => "S",
            "Vec<String>" => "Ss",
            "bool" => "Bool",
            _ => "N",
        },
        Span::call_site(),
    )
}
