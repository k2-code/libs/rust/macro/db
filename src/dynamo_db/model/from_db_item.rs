use crate::model::NamedFields;
use proc_macro2::{Ident, Span, TokenStream};
use quote::quote;

pub fn expand(name: &Ident, item_fields: &NamedFields) -> TokenStream {
    let mut conditioned_setters: Vec<TokenStream> = vec![];

    let av_container_name: Ident = get_av_container_name(name);

    for field in item_fields.iter() {
        let field_name: &Ident = field.ident.as_ref().unwrap();
        let field_name_txt: String = field_name.to_string();

        conditioned_setters.push(quote! {
            item.#field_name = match db_item.get(#field_name_txt) {
                Some(value) => #av_container_name(value.to_owned()).into(),
                _ => None,
            };
        });
    }

    let av_container_impls: TokenStream = expand_av_container(&av_container_name);

    quote! {
        #av_container_impls

        #[automatically_derived]
        impl From<std::collections::HashMap<String, aws_sdk_dynamodb::model::AttributeValue>> for #name {
            fn from(db_item: std::collections::HashMap<String, aws_sdk_dynamodb::model::AttributeValue>) -> #name {
                let mut item: #name = #name::default();

                #(#conditioned_setters)*

                item
            }
        }
    }
}

fn expand_av_container(av_container_name: &Ident) -> TokenStream {
    let numeric_types_impls: Vec<TokenStream> =
        get_from_av_container_numeric_types_impls(av_container_name);

    quote! {
        struct #av_container_name(aws_sdk_dynamodb::model::AttributeValue);

        impl From<#av_container_name> for Option<chrono::DateTime<chrono::Utc>> {
            fn from(avcn: #av_container_name) -> Option<chrono::DateTime<chrono::Utc>> {
                match avcn.0 {
                    aws_sdk_dynamodb::model::AttributeValue::S(value)
                        => match value.parse::<chrono::DateTime<chrono::Utc>>() {
                            Ok(date) => Some(date),
                            _ => None,
                        }
                    ,
                    _ => None,
                }
            }
        }

        impl From<#av_container_name> for Option<String> {
            fn from(avcn: #av_container_name) -> Option<String> {
                match avcn.0 {
                    aws_sdk_dynamodb::model::AttributeValue::S(value) => Some(value.to_owned()),
                    _ => None,
                }
            }
        }

        impl From<#av_container_name> for Option<Vec<String>> {
            fn from(avcn: #av_container_name) -> Option<Vec<String>> {
                match avcn.0 {
                    aws_sdk_dynamodb::model::AttributeValue::Ss(value) => Some(
                        value
                    ),
                    _ => None,
                }
            }
        }

        impl From<#av_container_name> for Option<bool> {
            fn from(avcn: #av_container_name) -> Option<bool> {
                match avcn.0 {
                    aws_sdk_dynamodb::model::AttributeValue::Bool(value) => Some(value),
                    _ => None,
                }
            }
        }

        impl<T> From<#av_container_name> for Option<types::db::JsonField<T>>
            where T: serde::de::DeserializeOwned {
            fn from(avcn: #av_container_name) -> Option<types::db::JsonField<T>> {
                match avcn.0 {
                    aws_sdk_dynamodb::model::AttributeValue::S(value) => {
                        let result = serde_json::from_str(&value);

                        if result.is_err() {
                            return None;
                        }

                        Some(types::db::JsonField(result.unwrap()))
                    },
                    _ => None,
                }
            }
        }

        #(#numeric_types_impls)*
    }
}

fn get_av_container_name(name: &Ident) -> Ident {
    Ident::new(&format!("AVContainer_{}", name), Span::call_site())
}

fn get_from_av_container_numeric_types_impls(av_container_name: &Ident) -> Vec<TokenStream> {
    let num_types = vec![
        "u8", "u16", "u32", "u64", "u128", "i8", "i16", "i32", "i64", "i128", "f32", "f64", "usize",
    ];

    num_types
        .iter()
        .map(|num_type| {
            let num_type_ident: Ident = Ident::new(num_type, Span::call_site());

            quote! {
                impl From<#av_container_name> for Option<#num_type_ident> {
                    fn from(avcn: #av_container_name) -> Option<#num_type_ident> {
                        match avcn.0 {
                            aws_sdk_dynamodb::model::AttributeValue::N(value) => Some(value.parse::<#num_type_ident>().unwrap()),
                            _ => None,
                        }
                    }
                }
            }
        })
        .collect()
}
